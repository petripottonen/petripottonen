<?php

// live site url for detection
$remoteSiteUrl = "petripottonen.com";

//local or remote variables
if(strpos($_SERVER['HTTP_HOST'], $remoteSiteUrl) !== false) {$environment = "remote";} else { $environment = "local";}

//modify your copy of WordPress to work with any domain name  -http://clickontyler.com/support/a/21/wordpress-liftoff/
$s = empty($_SERVER['HTTPS']) ? '' : ($_SERVER['HTTPS'] == 'on') ? 's' : '';
$port = ($_SERVER['SERVER_PORT'] == '80') ? '' : (":".$_SERVER['SERVER_PORT']);
// koska ei oo /wp/ ni kommentoitu tää versio ja käytetää seuraavaa
//$url = "http$s://" . $_SERVER['HTTP_HOST'] ."/oravastaging" . $port;
$url = "http$s://" . $_SERVER['HTTP_HOST'] . $port;
//$url = "http$s://" . $_SERVER['HTTP_HOST'] ."/wp" . $port;
//$url = "http$s://" . $_SERVER['HTTP_HOST'] . $port;
define('WP_HOME', $url);
define('WP_SITEURL', $url);
unset($s, $port, $url);


/** 
 * WordPressin perusasetukset.
 *
 * Tämä tiedosto sisältää seuraavat asetukset: MySQL-asetukset, Tietokantataulun etuliite,
 * henkilökohtaiset salausavaimet (Secret Keys), WordPressin kieli, ja ABSPATH. Löydät lisätietoja
 * Codex-sivulta {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php}. Saat MySQL-asetukset palveluntarjoajaltasi.
 *
 * Automaattinen wp-config.php-tiedoston luontityökalu käyttää tätä tiedostoa
 * asennuksen yhteydessä. Sinun ei tarvitse käyttää web-asennusta, vaan voit 
 * tallentaa tämän tiedoston nimellä "wp-config.php" ja muokata allaolevia arvoja.
 *
 * @package WordPress
 */


/* 
 * Unified variables 
 */   
//$hostname = 'localhost';  
$chartset = 'utf8';  
$collate = 'utf8_swedish_ci';  
$pl_hooks_php = 'true';  

/* 
 * Check for the current environment 
 */  

if($environment === "remote" ){
  $db_name = 'ketri_petripottonen';  
  $user_name = 'ketri_ketri'; 
  $password = 'usrJrzDqkJG6';  
  $hostname = 'localhost';  
}
else{
  $db_name = 'petripottonen';  
  $user_name = 'root'; 
  $password = 'root';  
  $hostname = 'localhost';  
}

  

// ** MySQL settings - You can get this info from your web host ** //  
/** The name of the database for WordPress */  
define('DB_NAME', $db_name);  
  
/** MySQL database username */  
define('DB_USER', $user_name);  
  
/** MySQL database password */  
define('DB_PASSWORD', $password);  
  
/** MySQL hostname */  
define('DB_HOST', $hostname);  
  
/** Database Charset to use in creating database tables. */  
define('DB_CHARSET', $chartset);  
  
/** The Database Collate type. Don't change this if in doubt. */  
define('DB_COLLATE', $collate);  



/** Pagelines enable PHP in hooks */  
define( 'PL_HOOKS_PHP', $pl_hooks_php);






/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Muuta nämä omiksi uniikeiksi lauseiksi!
 * Voit luoda nämä käyttämällä {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org palvelua}
 * Voit muuttaa nämä koska tahansa. Kaikki käyttäjät joutuvat silloin kirjautumaan uudestaan.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7+9M?Xruiwn=7y1(`VVub|mG+IF{qtxIr4$.xA^%(LKQ~_qOFi8F-uV[.GgM95@W');
define('SECURE_AUTH_KEY',  'hn!i&in%1BAf<%TXmi-Zir/<TKL.a`Pcnd7i!:*Wyfg +lCi9B^,UK/wFt9e4O0/');
define('LOGGED_IN_KEY',    ' 2n1/sN E1HE5+~]Oih,4C47t-Z8k{Sb5c^FfueH<H+|E2vp rS;W8;`L[J0+Q!%');
define('NONCE_KEY',        'b@uV!!pJDr%PT6C?M5eK3(iK.+Cq&z~(USq10pjZ1 2RS#xl:Vl*|;%GSv|AZFqi');
define('AUTH_SALT',        'r:g#*p]cDEs*g$.uc;t9c?|X+Ok^6`v f$A{[9@iQ|yVaR#b`1v1:;eXiR=-Jue9');
define('SECURE_AUTH_SALT', 'w2_-rEvjLKM?+!{JUr6>0%ldUAp{p;qv>kBWA=7aYi);S~uA|3U+ f.>;5.!|t(M');
define('LOGGED_IN_SALT',   '7** Hvbz@ND(+4~wynnZ_A3<b0N-0@|5UDv:,p|qQ>LK-IEus| Z[{*R%$.qx+,0');
define('NONCE_SALT',       'z(+-fn<CH6:T@u|l7t_208<]xi(|EDbnXxfr)<wy{y )kh>hH)%1#w=|3wE0%W+L');



/**#@-*/

/**
 * WordPressin tietokantataulujen etuliite (Table Prefix).
 *
 * Samassa tietokannassa voi olla useampi WordPress-asennus, jos annat jokaiselle
 * eri tietokantataulujen etuliitteen. Sallittuja merkkejä ovat numerot, kirjaimet
 * ja alaviiva _.
 *
 */
$table_prefix  = 'wp_Vz7uL_';

/**
 * WordPressin kieli.
 *
 * Muuta tämä WordPressin kieliasetusten muuttamiseksi. Vastaavasti nimetty 
 * kielitiedosto pitää asentaa hakemistoon wp-content/languages. Esimerkiksi,
 * asenna de.mo wp-content/languages -hakemistoon ja muuta WPLANG:in arvoksi 'de'
 * käyttääksesi WordPressiä saksan kielellä.
 */
define ('WPLANG', 'fi');

/**
 * Kehittäjille: WordPressin debug-moodi.
 *
 * Muuta tämän arvoksi true jos haluat nähdä kehityksen ajan debug-ilmoitukset
 * Tämä on erittäin suositeltavaa lisäosien ja teemojen kehittäjille.
 */
define('WP_DEBUG', false);

/* Siinä kaikki, älä jatka pidemmälle! */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
