<?php
/**
 * Danny's Skeleton child theme
 * Containing nothing other than 
 * the bare bone files 
 */

// Add your functions below



/* Disable the Admin Bar. */
//add_filter( 'show_admin_bar', '__return_false' );


// load Codekit CSS after style.css
/*
function high_priority_style() {
	printf( "<link rel='stylesheet' id='important-css'  href='%s' type='text/css' media='all' />\n", get_stylesheet_directory_uri() . '/codekit/codekit.css' );
}
add_action('wp_head', 'high_priority_style', 8);
*/


// Add excerpt support for pages
/*)
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}
*/

/*// bigger priority loads later. default = 10
add_action('the_post', 'postFunction', 10);
    function postFunction() { 
      echo "<h1>pdf link here</h1>";
}

*/

//http://wordpress.org/support/topic/error-404-on-pagination-when-changing-posts_per_page-on-query_posts#post-1553417
function remove_page_from_query_string($query_string)
{
if ($query_string['name'] == 'page' && isset($query_string['page'])) {
unset($query_string['name']);
// 'page' in the query_string looks like '/2', so i'm spliting it out
list($delim, $page_index) = split('/', $query_string['page']);
$query_string['paged'] = $page_index;
}
return $query_string;
}
// I will kill you if you remove this. I died two days for this line
add_filter('request', 'remove_page_from_query_string');





/*function theme_slug_filter_the_title( $title ) {
    $custom_title = 'YOUR CONTENT GOES HERE';
    $title .= $custom_title;
    return $title;
}

if ( is_main_query() {
  # code...
}
add_filter( 'the_title', 'theme_slug_filter_the_title' );

*/

//Add scripts to head
add_action('wp_footer', 'footerScripts', 10);
function footerScripts() { ?>

<script src="//use.typekit.net/dtu4fcd.js"></script>
<script>try{Typekit.load();}catch(e){}</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43126823-1', 'auto');
  ga('send', 'pageview');

</script>


<!--<script src="http://freqdec.github.io/slabText/js/jquery.slabtext.min.js"></script>

<script>
    // Function to slabtext the H1 headings
    function slabTextHeadlines() {
        jQuery("h1.slab").slabText({
            // Don't slabtext the headers if the viewport is under 380px
            "viewportBreakpoint":380,
            "maxFontSize": 999
        });


    };
    
    // Called one second after the onload event for the demo (as I'm hacking the
    // fontface load event a bit here)

    // Please do not do this in a production environment - you should really use
    // google WebFont loader events (or something similar) for better control
    jQuery(window).load(function() {
        // So, to recap... don't actually do this, it's nasty!
        setTimeout(slabTextHeadlines, 1000);
    });
  </script>   -->


<script>
jQuery(window).load(function() {     
if(window.location.hash) {
      var hash = window.location.hash; //Puts hash in variable
       setTimeout(function() {
jQuery("html, body").animate({scrollTop:jQuery(hash).offset().top -20 }, '500', 'swing', function() { 
   //alert("Finished animating");
});
    }, 100);

  }

   


      
    });
</script>
<script>



  /*  jQuery(document).ready(function() {
        //jQuery('html, body').animate({scrollTop:jQuery('#kuvaus').position().top}, 'slow');
        //jQuery("html, body").delay(500).animate({scrollTop: jQuery('#kuvauksen').offset().top }, 2000);
        jQuery('html, body').animate({
        scrollTop: jQuery("#riskienhallinta").offset().top
    }, 200);*/

       /* jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#kuvaus').offset().top
        }, -10, 'swing', function () {
           // window.location.hash = target;
        });*/
    });
</script>


<script>


</script>

<!-- readmore-->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/readmore.min.js"></script>
<script>

LoadreadMoreJs = function(classes, montakoRivia){

height = Math.round(montakoRivia*22.5);

jQuery(classes).readmore({
    speed: 300,
    maxHeight:height,
    moreLink: '<div class="readmore-link-container"><a class="smallcapslink" href="#"><i class="fa fa-plus-square"></i> Lue lisää</a></div>',
    lessLink: '<div class="readmore-link-container"><a class="smallcapslink" href="#"><i class="fa fa-minus-square"></i> Sulje</a></div>'
  });
}





//Orava Asuntorahasto -sivu
LoadreadMoreJs(".readmore.orava-asuntorahasto .hentry", 7);
LoadreadMoreJs(".readmore.historia .hentry", 7);
LoadreadMoreJs(".readmore.hallinto .hentry", 8);
LoadreadMoreJs(".readmore.riskienhallinta .hentry", 6);

 //Sijoitustoiminta -sivu
LoadreadMoreJs(".readmore.tavoitteet .hentry", 6);
LoadreadMoreJs(".readmore.sijoitusstrategia .hentry", 16.5); // Headings break the rhythm. In the mobile it breaks unless a small hack


 //LoadreadMoreJs(".readmore .hentry", 100);


	/*jQuery('.readmore .hentry').readmore({
	  speed: 300,
	  maxHeight: 200,
	  moreLink: '<div class="readmore-link-container"><a class="smallcapslink" href="#"><i class="fa fa-plus-square"></i> Lue lisää</a></div>',
		lessLink: '<div class="readmore-link-container"><a class="smallcapslink" href="#"><i class="fa fa-minus-square"></i> Sulje</a></div>'
	});*/
</script>


<!--lazyload-->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.lazyload.min.js"></script>
<script>
	jQuery("img.lazy").lazyload();
</script>
<?php }








/*------------------------------------*\
    WPML FUNCTIONS
\*------------------------------------*/

/*
//WPML shortcodes
function wpml_translate( $atts, $content = null ) {
    extract(shortcode_atts(array('lang'      => '',), $atts));
    $lang_active = ICL_LANGUAGE_CODE;   
    if($lang == $lang_active){
        return $content;
    }
}

add_shortcode("wpml_translate", "wpml_translate");
*/

/*
// WPML dont load useless stuff
define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);*/


// add shortened language codes to the navbar
function new_nav_menu_items309676($items,$args) {
    if (function_exists('icl_get_languages') && $args->theme_location == 'navi_nav') { // 'navi_nav', 'mobile_nav', 'main_nav', 'simple_nav', 'socialinks_nav',
        $languages = icl_get_languages('skip_missing=0');
        if(1 < count($languages)){
            foreach($languages as $l){
                if( $l['active'] != 1 ) { 
                    $items = $items.'<li class="menu-item-'.$l['language_code'].'"><a href="'. $l['url'].'">'. strtoupper( $l['language_code'] ).'</a></li>'; // to capitalize, replace 'strtopupper' with 'ucfirst' 

                }
            }
        }
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'new_nav_menu_items309676',10,2 );

// add shortened language codes to the mobile nav
function new_nav_menu_items3096762($items,$args) {
    if (function_exists('icl_get_languages') && $args->theme_location == 'mobile_nav') { // 'navi_nav', 'mobile_nav', 'main_nav', 'simple_nav', 'socialinks_nav',
        $languages = icl_get_languages('skip_missing=0');
        if(1 < count($languages)){
            foreach($languages as $l){
                if( $l['active'] != 1 ) { 
                    $items = $items.'<li class="menu-item-'.$l['language_code'].'"><a href="'. $l['url'].'">'. strtoupper( $l['language_code'] ).'</a></li>';
                }
            }
        }
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'new_nav_menu_items3096762',10,2 );










/*
add_action('wp_head', 'loadDetectizr');
    function loadDetectizr() { ?>
    <!--kokko-->
 <script src=" <?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
 <script src=" <?php echo get_stylesheet_directory_uri(); ?>/js/detectizr.js"></script> 
<script>
	Detectizr.detect({detectScreen:false});
</script>

<!-- BACKSTRECT-->
<script>
//if not a crappy windows phone
if( !jQuery( "html" ).hasClass( "windowsphone" ) ){

// load backstrech.js
url = "http://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js";
jQuery.getScript(url)
	.done(function() { // backstrech loaded successfully, so initialize it:
		var backstretchImg = "/wp-content/themes/dms-child/img/iisa-crop-medium2.jpg";
  	//jQuery(document).ready(function() {
  		  jQuery("body").backstretch([backstretchImg]);
  	//});

  	// Refresh if windowHeight changes (iPhone safari scroll url-bar thinghy)
  	var lastWindowHeight = window.innerHeight; //define height of the window
  	(function() {        
	    var timer;
	    jQuery(window).bind('scroll',function () {
	        clearTimeout(timer);
	        timer = setTimeout( refresh , 150 );
	    });

	    var refresh = function () { // when user stops scrolling
	        if (lastWindowHeight != window.innerHeight) { //if the height of window has changed (iOS safari address-bars appear/disappear)
	    		jQuery.backstretch([backstretchImg]); //then we'll call backstrech to make the bg right for new window size
	    		lastWindowHeight = window.innerHeight; //and update the last known window height so we can compare it again to the possibly new winwod height
	    		}
	    };
		})();
	})
	.fail(function() {
		// boo, fall back to something else 
		//alert (" backstretch loading failed");
});
}
</script>
<!-- BACKSTRECT END-->



    <?php
}

add_action('pagelines_page', 'detectizrAndBackstretch', 0);
    function detectizrAndBackstretch() { ?>

    	*/